#IMC

from itertools import zip_longest

peso=(77,70,80,60,68,56,97,72,95,50)
altura=(1.65,1.85,1.90,1.40,1.86,1.78,1.62,1.80,1.65,1.90)

IMC = [x / y**2 for x, y in zip_longest(peso,altura)]

print(IMC)
